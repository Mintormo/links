from django.conf.urls import include, url
from django.contrib import admin
import main.views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls), name = 'admin_page'),
    url(r'^login/$', main.views.login, name = 'login_page'),
    url(r'^logout/$', main.views.quit, name = 'logout'),
    url(r'^registration/$', main.views.register, name = 'reg_page'),
    url(r'^get/(?P<id>([0-9A-Za-f]{8})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{12}))/(?P<page>\d+)/$',
        main.views.get_item, name = 'get_item_page'),
    url(r'^addlink/$', main.views.add_link, name = 'add_link'),
    url(r'^addfolder/$', main.views.add_folder, name = 'add_folder'),
    url(r'^delete/$', main.views.delete_item, name = 'delete'),
    url(r'^change/(?P<id>([0-9A-Za-f]{8})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{12}))/$',
        main.views.edit_item, name = 'edit'),
    url(r'move/$', main.views.moveto, name = 'moveto'),
    url(r'^$', main.views.index, name = 'root_page'),
]
