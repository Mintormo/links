from django.contrib import admin
from main.models import Directory, Links

admin.site.register(Directory)
admin.site.register(Links)
