from django import forms
from main import models


class LoginForm(forms.Form):
    username = forms.CharField(max_length=100,
                               widget=forms.TextInput(attrs={"placeholder":"Логин","class":"uk-width-1-1;"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Пароль","class":"uk-width-1-1;"}))

class RegisterForm(forms.Form):
    username = forms.CharField(max_length=100,
                               widget=forms.TextInput(attrs={"placeholder":"Логин","class":"uk-width-1-1;"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Пароль","class":"uk-width-1-1;"}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Повторите пароль","class":"uk-width-1-1;"}))
    email = forms.CharField(widget=forms.EmailInput(attrs={"placeholder":"Почтовый ящик","class":"uk-width-1-1;"}))

class AddOrChangeLinkForm(forms.Form):
    title = forms.CharField(max_length=256, label="Название",
                            widget=forms.TextInput(attrs={"required":"required"}))
    link = forms.URLField(label="Ссылка",
                          widget=forms.URLInput(attrs={"required":"required"}))
    description = forms.CharField(label="Комментарий", required=False,
                                  widget=forms.Textarea(attrs={"rows":5}))
    tags = forms.CharField(label="Теги", max_length=1024, required=False,
                           widget=forms.TextInput(attrs={"placeholder":"Разделять пробелом"}))

class AddOrChangeFolderForm(forms.Form):
    title = forms.CharField(max_length=256, label="Название",
                            widget=forms.TextInput(attrs={"required":"required"}))
    description = forms.CharField(label="Комментарий", required=False,
                                  widget=forms.Textarea(attrs={"rows":5}))