# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Directory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(null=True)),
                ('parent', models.UUIDField(null=True)),
                ('title', models.CharField(max_length=256, unique=True, default='2015-06-28 01:10:37.411717')),
                ('description', models.TextField(max_length=1024)),
                ('tags', models.CharField(max_length=1024, default='')),
                ('date_of_adding', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Links',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.URLField(unique=True, default='/')),
                ('status', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.AddField(
            model_name='directory',
            name='link',
            field=models.ForeignKey(null=True, to='main.Links'),
        ),
        migrations.AddField(
            model_name='directory',
            name='owner',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
