# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='directory',
            name='date_of_adding',
            field=models.DateField(default=datetime.datetime(2015, 6, 27, 22, 13, 30, 5589, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='directory',
            name='title',
            field=models.CharField(max_length=256, default='2015-06-28 01:13:30.004589', unique=True),
        ),
    ]
