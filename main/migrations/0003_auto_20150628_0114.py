# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150628_0113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='directory',
            name='date_of_adding',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='directory',
            name='title',
            field=models.CharField(max_length=256, default='2015-06-28 01:14:11.904986', unique=True),
        ),
    ]
