# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20150628_0114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='directory',
            name='date_of_adding',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='directory',
            name='title',
            field=models.CharField(default='2015-08-23 17:12:25.312025', unique=True, max_length=256),
        ),
    ]
