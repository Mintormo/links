# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20150823_1712'),
    ]

    operations = [
        migrations.AlterField(
            model_name='directory',
            name='title',
            field=models.CharField(default='2015-09-17 22:58:04.675739', max_length=256),
        ),
    ]
