import datetime
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

class Links(models.Model):
    link = models.URLField(default='/', unique=True)
    status = models.PositiveIntegerField(default=0) # 0 - нормальная ссылка, 1 - битая

    def __str__(self):
        return self.link

class Directory(models.Model):
    owner = models.ForeignKey(User, null=True)
    uuid = models.UUIDField(null=True)
    parent = models.UUIDField(null=True, unique=False)
    link = models.ForeignKey(Links, null=True)
    title = models.CharField(max_length=256, default=str(datetime.datetime.now()))
    description = models.TextField(max_length=1024)
    tags = models.CharField(max_length=1024, default='')
    date_of_adding = models.DateField(null=True, auto_now_add=True)

    def __str__(self):
        return self.owner.username+ ": " +self.title




