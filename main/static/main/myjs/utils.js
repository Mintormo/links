"use strict";
function edit_link_or_folder()
{
    if ($("input:checkbox:checked").length > 0) {
        if ($("input:checkbox:checked").length == 1) {
            if (!$("#all").prop("checked")) {
               $.get("/change/"+$("input:checkbox:checked").prop("id")+"/", {}, function(data) {
                   var d = JSON.parse(data);
                   if ("link" in d) {
                       $("#add_or_change_link").find("#add_or_change_link_title").text("Редактировать ссылку");
                       $("#add_or_change_link").find("#add_or_change_link_form").prop("action",
                                                        "/change/"+$("input:checkbox:checked").prop("id")+"/");
                       $("#add_or_change_link").find("#add_or_change_link_form").find("#submit").text("Изменить");
                       $("#add_or_change_link").find("#add_or_change_link_form").find("#id_title").val(d.title);
                       $("#add_or_change_link").find("#add_or_change_link_form").find("#id_link").val(d.link);
                       $("#add_or_change_link").find("#add_or_change_link_form").find("#id_description").val(d.description);
                       $("#add_or_change_link").find("#add_or_change_link_form").find("#id_tags").val(d.tags);
                       $("#add_link").click();
                   } else {
                       $("#add_or_change_folder").find("#add_or_change_folder_title").text("Редактировать данные");
                       $("#add_or_change_folder").find("#add_or_change_folder_form").prop("action",
                                                        "/change/"+$("input:checkbox:checked").prop("id")+"/");
                       $("#add_or_change_folder").find("#id_title").val(d.title);
                       $("#add_or_change_folder").find("#id_description").text(d.description);
                       $("#add_or_change_folder").find("#submit").text("Изменить");
                       $("#add_folder").click();
                   }
               });
            } else {
              // TODO: Обработать ситуацию когда выбраны все ссылки на странице
            }
        } else {
            alert("За один раз можно редактировать только одну ссылку!");
        }
    } else {
        alert("Не выбрано ни одной ссылки!");
    }

}

function delete_link_or_folder()
{
    if(confirm("Удалить?")) {
        if ($("input:checkbox:checked").length > 0) {
            if ($("#all").prop("checked")) {

            } else {
               var checked = {"id":""};

               $("input:checkbox:checked").each(function(i, v) {
                    if (i == 0)
                        checked.id += v.id;
                    else
                        checked.id += "," + v.id;
               });

               $.ajaxSetup({
                   beforeSend: function(xhr, settings) {
                       if (!this.crossDomain) {
                           xhr.setRequestHeader("X-CSRFToken", $.cookie("csrftoken"));
                       }
                   }
               });

               $.post("/delete/", checked, function(data) {
                   if (data == "ok")
                       location.reload();
                   else
                       alert("Ошибка: Не удалось выполнить удаление!");
               });
            }

        } else {
            alert("Не выбрано ни одной ссылки!");
        }
    } else {
    }
}

function add_link()
{
    $("#add_or_change_link").find("#add_or_change_link_title").text("Добавить ссылку");
    $("#add_or_change_link").find("#submit").text("Добавить");
    $("#add_or_change_link").find("#add_or_change_link_form").prop("action", "/addlink/");
    $("#add_or_change_link").find("#id_title").val("");
    $("#add_or_change_link").find("#id_link").val("");
    $("#add_or_change_link").find("#id_description").val("");
    $("#add_or_change_link").find("#id_tags").val("");
    $("#add_link").click();
}

function add_folder()
{
    $("#add_or_change_folder").find("#add_or_change_folder_title").text("Добавить папку");
    $("#add_or_change_folder").find("#id_title").val("");
    $("#add_or_change_folder").find("#id_description").text("");
    $("#add_or_change_folder").find("#add_or_change_folder_form").prop("action", "/addfolder/");
    $("#add_or_change_folder").find("#submit").text("Добавить");
    $("#add_folder").click();
}

function get_data_for_move_links()
{
    var data = {"checked":"","selected":""};

    $("input:checkbox:checked:not(#all)").each(function (i,v) {
        if (i == 0) {
            data.checked += v.id;
        } else {
            data.checked += "," + v.id;
        };
    });
    data.selected += $("div#folders").fancytree("getTree").getActiveNode().key;

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
           if (!this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", $.cookie("csrftoken"));
           }
        }
    });

    var d = "checked=" + data.checked + "&" + "selected=" + data.selected;
    $.post("/move/", d, function(data) {
        if (data == "bad")
            alert("Ошибка! Не удалось выполнить перемещение!");
        else
            window.location.reload(true);
    });
}

function move_links()
{
    var s = $("input:checkbox:checked:not(#all)");

    if (s.length == 0) {
        alert("Не выбрано ни одного элемента!");
        return;
    }

    $("#move_links").click();
}