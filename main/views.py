import uuid
from django.contrib import auth
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage
from django.core.urlresolvers import reverse
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from django.views.decorators.http import require_POST, require_GET
from main import forms
from main.forms import AddOrChangeLinkForm, AddOrChangeFolderForm
from main.models import Directory, Links
from django.utils import timezone
import re


@login_required(login_url = '/login/', redirect_field_name='')
def index(request):
    def get_folders_line(user=None, id=""):
        res = []
        root_uuid = str(Directory.objects.get(owner=user, parent=None, link=None).uuid)
        it = Directory.objects.get(owner=user, uuid=id, link=None)
        while str(it.uuid) != root_uuid:
            res.append([it.title, it.uuid])
            it = Directory.objects.get(owner=user, uuid=it.parent, link=None)
        res = res[::-1]
        return res

    def get_folders_tree(user=None, id=""):
        res = []

        if (id == "") or (user == None):
            return None
        else:
            items = Directory.objects.filter(owner=user, parent=id, link=None)
            for i in items:
                res.append([i.title, str(i.uuid), get_folders_tree(user, i.uuid)])
            return res

    args = {}
    args["username"] = request.user.username
    args["title"] = "segm.ru"
    args["add_form"] = AddOrChangeLinkForm()
    args["add_folder"] = AddOrChangeFolderForm()

    if "state" in request.session:
        state = request.session["state"]
    else:
        root = Directory.objects.get(owner=request.user, parent=None)
        request.session["state"] = root.uuid
        state = request.session["state"]
    args["state"] = state

    if "page" in request.session:
        page = request.session["page"]
    else:
        page = 1

    items = Directory.objects.filter(owner=request.user, parent=state)

    folders = items.filter(link=None)
    links = items.exclude(link=None)

    items = []
    items.extend(folders)
    items.extend(links)

    p = Paginator(items, 20)

    try:
        args["items"] = p.page(page)
    except EmptyPage:
        args["items"] = p.page(p.num_pages)

    args["root_uuid"] = str(Directory.objects.get(owner=request.user, parent=None).uuid)
    folders = get_folders_tree(request.user, args["root_uuid"])
    args["folders_tree"] = folders
    s = get_folders_line(request.user, request.session["state"])
    args["folders_line"] = s

    return render(request, "main/main.html", args)

def login(request):
    args = {}
    args.update(csrf(request))
    args["title"] = "segm.ru"

    if request.POST:
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            user = auth.authenticate(username=form.cleaned_data["username"],
                                     password=form.cleaned_data["password"])
            if user is not None and user.is_authenticated():
                auth.login(request, user)
                root = Directory.objects.get(owner=request.user, parent=None)
                request.session["state"] = str(root.uuid)
                return redirect(reverse("root_page"))
            else:
                return redirect(reverse("login_page"))
        else:
            args["form"] = form
            return render(request, "main/login.html", args)

    form = forms.LoginForm()
    args["form"] = form
    return render(request, "main/login.html", args)

def register(request):
    args = {}
    args.update(csrf(request))
    args["title"] = "segm.ru"

    if request.POST:
        form = forms.RegisterForm(request.POST)
        if form.is_valid():
            exist_user = User.objects.filter(username=form.cleaned_data["username"])
            if len(exist_user) > 0:
                new_form = forms.RegisterForm(request.POST)
                args["form"] = new_form
                return render(request,"main/register.html",args)

            new_user = User.objects.create_user(form.cleaned_data["username"],
                                                form.cleaned_data["email"],
                                                form.cleaned_data["password"])
            new_user.is_active = True
            new_directory = Directory.objects.create(owner=new_user, uuid=uuid.uuid4())

            return redirect(reverse("root_page"))
        else:
            new_form = forms.RegisterForm(request.POST)
            args["form"] = new_form
            return render(request,"main/register.html",args)

    form = forms.RegisterForm()
    args["form"] = form
    return render(request, "main/register.html", args)

def quit(request):
    logout(request)
    return redirect(reverse("root_page"))

@login_required(login_url = '/login/', redirect_field_name='')
@require_GET
def get_item(request, id='', page=1):
    item = Directory.objects.get(owner=request.user, uuid=id)
    if item is not None:
        request.session["state"] = id
    request.session["page"] = page
    return redirect(reverse("root_page"))

@login_required(login_url = '/login/', redirect_field_name='')
@require_POST
def add_link(request):
    form = AddOrChangeLinkForm(request.POST)
    if form.is_valid():
        is_exist_link = Links.objects.filter(link=form.cleaned_data["link"])
        is_exist_title = Directory.objects.filter(owner=request.user,
                                                  parent=request.session["state"],
                                                  title=form.cleaned_data["title"])

        title = None
        if not is_exist_title.exists():
            title = form.cleaned_data["title"]
        else:
            title = "Копия " + form.cleaned_data["title"]

        link = None
        if not is_exist_link.exists():
            link = Links.objects.create(link=form.cleaned_data["link"])
        else:
            link = is_exist_link[0]

        dir_item = Directory.objects.create(owner=request.user,
                                            uuid=uuid.uuid4(),
                                            parent=request.session["state"],
                                            link=link,
                                            title=title,
                                            tags=form.cleaned_data["tags"],
                                            description=form.cleaned_data["description"],
                                            date_of_adding=timezone.now())

        return redirect(reverse("root_page"))
    else:
        args = {}
        args.update(csrf(request))
        return redirect(reverse("root_page"), args)

@login_required(login_url = '/login/', redirect_field_name='')
@require_POST
def add_folder(request):
    form = AddOrChangeFolderForm(request.POST)
    if form.is_valid():
        is_exist_title = Directory.objects.filter(owner=request.user,
                                                  parent=request.session["state"],
                                                  title=form.cleaned_data["title"])

        title = None
        if not is_exist_title.exists():
            title = form.cleaned_data["title"]
        else:
            title = "Копия " + form.cleaned_data["title"]

        dir_item = Directory.objects.create(owner=request.user,
                                            uuid=uuid.uuid4(),
                                            parent=request.session["state"],
                                            link=None,
                                            title=title,
                                            tags="",
                                            description=form.cleaned_data["description"],
                                            date_of_adding=timezone.now())
        return redirect(reverse("root_page"))
    else:
        args = {}
        args.update(csrf(request))
        return redirect(reverse("root_page"), args)

@login_required(login_url = '/login/', redirect_field_name='')
@require_POST
def delete_item(request):
    list_id = request.POST["id"]
    ids = list_id.split(",")
    for item in ids:
        res = re.match(r"^([0-9A-Za-f]{8})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{12})$",
                           item)
        if res == None:
            return HttpResponse("bad")

    for item in ids:
        try:
            Directory.objects.get(owner=request.user, uuid=item).delete()
        except Directory.DoesNotExist:
            return HttpResponse("bad")
    return HttpResponse("ok")

@login_required(login_url = '/login/', redirect_field_name='')
def edit_item(request, id):
    if request.POST:
        item = Directory.objects.get(owner=request.user, uuid=id)
        if item.link is not None:
            form = AddOrChangeLinkForm(request.POST)
            if form.is_valid():
                item.title = form.cleaned_data["title"]
                test_title = Directory.objects.filter(owner=request.user,
                                                      parent=request.session["state"],
                                                      title=item.title)
                if test_title.exists():
                    item.title = "Копия " + item.title

                test_link = Links.objects.filter(link=form.cleaned_data["link"])
                if test_link.exists():
                    item.link = test_link[0]
                else:
                    Links.objects.create(link=form.cleaned_data["link"])
                item.tags = form.cleaned_data["tags"]
                item.description = form.cleaned_data["description"]
                item.save()
        else:
            form = AddOrChangeFolderForm(request.POST)
            if form.is_valid():
                item.title = form.cleaned_data["title"]
                test_title = Directory.objects.filter(owner=request.user,
                                                      parent=request.session["state"],
                                                      title=item.title)
                if test_title.exists():
                    item.title = "Копия " + item.title

                item.description = form.cleaned_data["description"]
                item.save()
        return redirect(reverse("root_page"))
    else:
        item = Directory.objects.get(owner=request.user, uuid=id)
        if item:
            if item.link is not None:
                res = {"title": item.title, "link": item.link.link, "description": item.description, "tags": item.tags}
            else:
                res = {"title": item.title, "description": item.description}
            return HttpResponse(JsonResponse(res).content)
        else:
            return HttpResponse(JsonResponse('{"title":"bad"}').content)

@login_required(login_url = '/login/', redirect_field_name='')
def moveto(request):
    chk = request.POST["checked"].split(",")
    for item in chk:
        res = re.match(r"^([0-9A-Za-f]{8})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{12})$",
                           item)
        if res == None:
            return HttpResponse("bad")

    sel = request.POST["selected"]
    res = re.match(r"^([0-9A-Za-f]{8})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{4})\-([0-9A-Za-f]{12})$",
                       sel)
    if res == None:
        return HttpResponse("bad")

    for i in chk:
        it = Directory.objects.get(owner=request.user, uuid=i)
        it.parent = sel
        it.save()

    return redirect(reverse('root_page'))
