import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dp.settings")
django.setup()
from main.models import Directory

all_deleted = 0

def clear():
    global all_deleted
    count = 0

    it = Directory.objects.all().exclude(parent=None)
    if len(it) == 0:
        return False

    for i in it:
        l = Directory.objects.filter(uuid=i.parent)
        if not l.exists():
            i.delete()
            count = count + 1

    all_deleted += count
    if count > 0:
        return True
    else:
        return False

while clear():
    pass

print("Удалено {} объектов.".format(all_deleted))



